// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/tasks');

// [SECTION] Routing Component
	const route = express.Router();

// [SECTION] Tasks Routes
	// Create Task
	route.post('/', (req, res) => {
		let taskInfo = req.body
		controller.createTask(taskInfo).then(result => res.send(result)
		)
	});

	// Retrieve all tasks
	route.get('/', (req, res) => {
		controller.getAllTasks().then(result => 
		{
			res.send(result);
		})
	});

	// Retrieve a single task
	route.get('/:id', (req, res) => {
		let taskId = req.params.id;
		controller.getTask(taskId).then(outcome => {
			res.send(outcome);
		})
	});

	// Delete a single task
	route.delete('/:id', (req, res) => {
		let taskId = req.params.id;
		controller.deleteTask(taskId).then(outcome => {
			res.send(outcome);
		})
	})

	// Update a task
	route.put('/:id', (req, res) => {
		let id = req.params.id;
		let taskBody = req.body
		controller.updateTask(id, taskBody).then(outcome => {
			res.send(outcome)
		})
	})



// [SECTION] Expose Route System 
	module.exports = route;

