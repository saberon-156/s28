// [SECTION] Dependencies and Modules
	const exp = require('express');
	const controller = require('../controllers/users');

// [SECTION] Routing Component
	const route = exp.Router()

// [SECTION] User Routes
	route.post('/register', (req, res) => {
		let data = req.body;
		controller.registerUser(data).then(result => res.send(result));
	})

	// Retrieve all users
	route.get('/', (req, res) => {
		controller.getAllUsers().then(result => {
			res.send(result);
		});
	});
	
	// Retrieve user profile
	route.get('/:id', (req, res) =>{
		let userId = req.params.id;
		controller.getProfile(userId).then(outcome => {
			res.send(outcome);
		})
	});

	// Delete user profile
	route.delete('/:id', (req, res) => {
		let userId = req.params.id;
		controller.deleteUser(userId).then(outcome => {
			res.send(outcome);
		})
	});

	// Update user profile
	route.put('/:id', (req, res) => {
		let id = req.params.id;
		let katawan = req.body
		controller.updateUser(id, katawan).then(outcome => {
			res.send(outcome)
		})
	})

// [SECTION] Expose Route System 
	module.exports = route;
