// [SECTION] Dependencies and Modules
const Task = require('../models/Task')

// [SECTION] Functionalities

	// Create New Task
	module.exports.createTask = (clientInput) => {
		let taskName = clientInput.name
		let newTask = new Task({
			name: taskName
		});
		return newTask.save().then((task, error) => {
			if (error) {
				return 'Saving New Task Failed.'
			} else {
				return 'A New Task has been Created!'
			}

		})
	};
	// Retrieve All Tasks
	module.exports.getAllTasks = () => {
		return Task.find({}).then(searchResult => {
			return searchResult;
		})
	};

	// Retrieve Single Task
	module.exports.getTask = (data) => {
		return Task.findById(data).then(result => {
			return result;
		})
	};

	// Delete task
	module.exports.deleteTask = (taskId) => {
		return Task.findByIdAndRemove(taskId).then((removedTask, err) => {
			if (removedTask) {
				return `${removedTask}
						Task Deleted Successfully.`
			} else {
				return `No accounts were removed.`
			}
		})
	};

	// Update Task
	module.exports.updateTask = (taskId, newContent) => {
		let newStatus = newContent.status;
		return Task.findById(taskId).then((foundTask, error) => {
			if (foundTask) {
				foundTask.status = newStatus;
				return foundTask.save().then((updatedTask, saveErr) => {
					if (saveErr) {
						return false;
					} else {
						return updatedTask;
					}
				})
			} else {
				return 'No task found.';

			}
		})
	};
