// Create a Schema, model for thr task collection, and make sure to export.
const mongoose = require('mongoose');

const taskBlueprint = new mongoose.Schema({
	name: {
		type:String,
		required: [true, 'Task name is Required.']
	},
	status: {
		type: String,
		default: 'pending'
	}
});

// create a model using the Schema
module.exports = mongoose.model("Task", taskBlueprint);